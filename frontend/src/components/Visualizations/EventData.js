export const EventData = () => {
    return {
        "2020-03": 3,
        "2020-10": 87,
        "2020-11": 144,
        "2020-12": 57,
        "2021-01": 40,
        "2021-02": 44,
        "2021-03": 86,
        "2021-04": 122,
        "2021-05": 131,
        "2021-06": 226,
        "2021-07": 280,
        "2021-08": 307,
        "2021-09": 192,
        "2021-10": 124,
        "2021-11": 32,
        "2021-12": 18,
        "2022-01": 9,
        "2022-02": 8,
        "2022-03": 6,
        "2022-04": 16,
        "2022-06": 8
    }
}