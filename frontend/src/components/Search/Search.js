import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";
import "../../assets/styles/Search.css"

import Events from "../Events/Events";
import News from "../News/News"
import Venues from "../Venues/Venues"

export const Search = () => {
    const props = useLocation();
    const search = props.state.searchQuery;
    const [query, setQuery] = useState(search);
    useEffect(() => {setQuery(search)}, [search]);

    return(
        <>
            <div>
                <h2> Search</h2>
                <br></br>
                <Events query={query}/>
                <br></br>
                <News query={query}/>
                <br></br>
                <Venues query={query}/>
            </div>
        </>
    )
}

export default Search;
