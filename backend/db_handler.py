import mysql.connector
import secret_stuff
from urllib import parse

PAGE_SIZE = 10

def retrieve_page_and_count(table_name, query_params):
    cnx = mysql.connector.connect(host=secret_stuff.host,
                                  port=secret_stuff.port,
                                  user=secret_stuff.user,
                                  database=secret_stuff.database,
                                  password=secret_stuff.password)
    cursor = cnx.cursor()

    # Initialize query to select all and a count of number of results
    query = "select *, count(*) over() from {}".format(table_name)

    # Add relevant filters for each model
    if table_name == 'venues':
        query = query + venues_filter_info(query_params)
    elif table_name == 'events':
        query = query + events_filter_info(query_params)
    elif table_name == 'articles':
        query = query + articles_filter_info(query_params)
    else:
        raise Exception("Unexpected error occurred: invalid table_name '{}'"
                        .format(table_name))

    # Add sorting info to query
    query = query + sort_info(query_params)

    # Add sorting info to query
    query = query + page_info(query_params)

    # Make Query
    cursor.execute(query)
    return cursor.fetchall()


def retrieve_instance(table_name, row_id):
    cnx = mysql.connector.connect(host=secret_stuff.host,
                                  port=secret_stuff.port,
                                  user=secret_stuff.user,
                                  database=secret_stuff.database,
                                  password=secret_stuff.password)
    cursor = cnx.cursor()

    # Make query
    query = "select * from {} where id={}".format(table_name, row_id)
    cursor.execute(query)
    return cursor.fetchall()


def retrieve_related_models(desired_table_name, venue_id):
    cnx = mysql.connector.connect(host=secret_stuff.host,
                                  port=secret_stuff.port,
                                  user=secret_stuff.user,
                                  database=secret_stuff.database,
                                  password=secret_stuff.password)
    cursor = cnx.cursor()
    query = "select * from {} where venue_id=\"{}\"".format(desired_table_name,
                                                            venue_id)
    cursor.execute(query)
    return cursor.fetchall()

def page_info(params):
    # Set a page number
    page_number = 1
    if params.get('page_number'):
        page_number = int(params.get('page_number'))

    # Page math
    start = PAGE_SIZE * (page_number - 1)

    # Base SQL Query
    return " LIMIT {} OFFSET {}".format(PAGE_SIZE, start)


def sort_info(params):
    # If desired, sort query results
    if params.get('orderBy'):
        if params.get('orderDirection'):
            order_direction = params.get('orderDirection')
        else:
            order_direction = "ASC"
        return " ORDER BY {} {}".format(params.get('orderBy'), order_direction)
    else:
        return ""


def venues_filter_info(params):
    filters = " WHERE 1=1"

    for param in ('name', 'city', 'zipcode'):
        if param in params:
            new_filter = " AND {} = '{}'"\
                            .format(param, parse.unquote(params[param]))
            filters = filters + new_filter

    if 'hasAccessibleSeatingInfo' in params:
        op = '!=' if params['hasAccessibleSeatingInfo'] == 'true' else '='
        filters = filters + " AND accessible_seating_info {} 'N/A'".format(op)

    if 'hasParkingInfo' in params:
        op = '!=' if params['hasParkingInfo'] == 'true' else '='
        filters = filters + " AND parking_info {} 'N/A'".format(op)

    if 'query' in params:
        filters = filters + " AND name LIKE '%{}%'".format(params['query'])
    return filters


def events_filter_info(params):
    if 'newsQuantity' in params:
        filters = " JOIN articles ON articles.venue_id = events.venue_id" \
                  " GROUP BY events.id HAVING COUNT(articles.id) {}"\
                    .format(parse.unquote(params['newsQuantity']))
    else:
        filters = " WHERE 1=1"

    for param in ('name', 'promoter'):
        if param in params:
            new_filter = " AND {} = '{}'"\
                            .format(param, parse.unquote(params[param]))
            filters = filters + new_filter

    if 'hasMedia' in params:
        op = 'NOT ' if params['hasMedia'] == 'true' else ''
        filters = filters + " AND {}(media is null OR media = '')".format(op)

    if 'date' in params:
        filters = filters + " AND date_time LIKE '{}%'"\
                            .format(parse.unquote(params['date']))

    if 'venue' in params:
        venue_id = get_venue_id_for_name(params['venue'])
        filters = filters + " AND venue_id = '{}'".format(venue_id)

    if 'query' in params:
        filters = filters + " AND name LIKE '%{}%'".format(params['query'])
    return filters


def articles_filter_info(params):
    if 'eventsQuantity' in params:
        filters = " JOIN events ON events.venue_id = articles.venue_id" \
                  " GROUP BY articles.id HAVING COUNT(events.id) {}"\
                    .format(parse.unquote(params['eventsQuantity']))
    else:
        filters = " WHERE 1=1"

    for param in ('source',):
        if param in params:
            new_filter = " AND {} = '{}'"\
                            .format(param, parse.unquote(params[param]))
            filters = filters + new_filter

    if 'hasMedia' in params:
        op = 'NOT ' if params['hasMedia'] == 'true' else ''
        filters = filters + " AND {}(media is null OR media = '')".format(op)

    if 'publicationDate' in params:
        filters = filters + " AND publication_date LIKE '{}%'"\
                            .format(parse.unquote(params['publicationDate']))

    if 'venue' in params:
        venue_id = get_venue_id_for_name(params['venue'])
        filters = filters + " AND venue_id = '{}'".format(venue_id)

    if 'query' in params:
        filters = filters + " AND title LIKE '%{}%'".format(params['query'])
    return filters


def get_venue_id_for_name(name):
    cnx = mysql.connector.connect(host=secret_stuff.host,
                                  port=secret_stuff.port,
                                  user=secret_stuff.user,
                                  database=secret_stuff.database,
                                  password=secret_stuff.password)
    cursor = cnx.cursor()

    query = "select venue_id from venues where name = '{}'".format(name)
    cursor.execute(query)
    rows = cursor.fetchall()
    if len(rows):
        return rows[0][0]
    return ''
