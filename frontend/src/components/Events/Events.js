import React, { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap/'
import useAxios from 'axios-hooks'
import EventCard from './EventCard'
import EventDropDown from './EventDropDown'
import EventSearchForm from './EventSearchForm'
import Pagination from "../Pagination/Pagination"
import "../../assets/styles/Pagination.css";
import "../../assets/styles/ModelViewer.css"


export const Event = ( props ) =>
{
    let defaultSearch = 'none';
    let searchPage = false;
    //if Event was called with a search query passed in, we are on the searchpage
    //this will affect whether filters and model search bars are displayed, and whether
    //the initial search is empty or instantiated to the query passed in
    if ( props.query )
    {
        defaultSearch = props.query;
        searchPage = true;
    }

    //pagination states to keep track of current page and current page window
    const [ currentPage, setCurrentPage ] = useState( 1 );
    const [ currentWindow, setCurrentWindow ] = useState( 0 );

    //search, sort filter variables which represent values to be passed into the api call.
    //these variables all useState such that when one is changed the page is re rendered
    const [ venueFilter, setVenueFilter ] = useState( 'none' );
    const [ promoterFilter, setPromoterFilter ] = useState( 'none' );
    const [ hasMediaFilter, setHasMediaFilter ] = useState( 'none' );
    const [ order, setOrder ] = useState( 'none' );
    const [ quantity, setQuantity ] = useState( 'none' );
    const [ dateFilter, setDateFilter ] = useState( 'none' );

    //search, sort, filter title states which update to represent which filters are being used
    //on the current page; title states are displayed on dropdowns
    const [ venueTitle, setVenueTitle ] = useState( 'venue' );
    const [ promoterTitle, setPromoterTitle ] = useState( 'promoter' );
    const [ mediaTitle, setMediaTitle ] = useState( 'media' );
    const [ orderTitle, setOrderTitle ] = useState( 'sort by' );
    const [ quantityTitle, setQuantityTitle ] = useState( 'related news' );
    const [ dateTitle, setDateTitle ] = useState( 'date' );

    // set up search state using useState and useEffect to capture any changes
    const [ search, setSearch ] = useState( defaultSearch );
    useEffect( () => { setSearch( defaultSearch ) }, [ defaultSearch ] );

    //scan through the search sort filter variables and append to API request variable
    //appropriately based on the values of the different filter variables
    let request = '';
    ( search !== 'none' && search !== '' ) ? request += `&query=${ search }` : request += '';
    ( venueFilter !== 'none' ) ? request += `&venue=${ venueFilter }` : request += '';
    ( promoterFilter !== 'none' ) ? request += `&promoter=${ promoterFilter }` : request += '';
    ( hasMediaFilter !== 'none' ) ? request += `&hasMedia=${ hasMediaFilter }` : request += '';
    ( order !== 'none' ) ? request += `&orderBy=${ order }` : request += '';
    ( quantity !== 'none' ) ? request += `&newsQuantity${ quantity }` : request += '';
    ( dateFilter !== 'none' ) ? request += `&date=${ dateFilter }` : request += '';

    //make api call by appending request, which is updated dynamically with useState
    const [ { data, loading } ] = useAxios( `/api/events?page_number=${ currentPage }` + request );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }
    // grab the data variable which has api call results and the number of pages
    const pagesData = data.data;
    const numPages = data.total_pages;
    let cardsRow = [];
    for ( let i = 0; i < pagesData.length; i++ )
    {
        //cardsRow is a list of up to 10 cards (length of pagesData) which store all information
        //regarding an event object/model
        cardsRow.push(
            <Col key={ pagesData[ i ][ "id" ] }>
                <EventCard search={ search } id={ pagesData[ i ][ "id" ] }
                    name={ pagesData[ i ][ "name" ] } link={ pagesData[ i ][ "link" ] }
                    date_time={ pagesData[ i ][ "date_time" ] } media={ pagesData[ i ][ "media" ] }
                    promoter={ pagesData[ i ][ "promoter" ] }
                    special_info={ pagesData[ i ][ "special_info" ] } />
            </Col> )
    }
    //structure which holds the filters and search bar for each model page; outputs them in one row
    //takes in a bool searchPage; if its true we are on the searchpage and don't need filters/search bar
    function FilterSearch ( searchPage )
    {
        if ( !searchPage )
        {
            return (
                <Row>
                    {/* promoter filter with popular suggestions and a search bar for any user specified filter*/ }
                    {<EventDropDown a='Search for a Promoter' name="promoter"
                        filters={ [ "all", "AEG LIVE", "BROADWAY IN HOLLYWOOD",
                            "FELD ENTERTAINMENT DISNEY ON ICE", "LIVE NATION MUSIC", "PROMOTED BY VENUE" ] }
                        setTitle={ setPromoterTitle } setFilter={ setPromoterFilter }
                        currentTitle={ promoterTitle } updatePage={ setCurrentPage } /> }
                    {/* venue filter with popular suggestions and a search bar for any user specified filter*/ }
                    {<EventDropDown a='Search for a Venue' name="venue"
                        filters={ [ "all", "Alamodome", "Madison Square Garden",
                            "The Met Philadelphia", "United Center", "CHI Health Center Omaha" ] }
                        setTitle={ setVenueTitle } setFilter={ setVenueFilter }
                        currentTitle={ venueTitle } updatePage={ setCurrentPage } /> }
                    {/* date filter with a search bar for any user specified filter*/ }
                    {<EventDropDown a='YYYY-MM-DD' name="date"
                        filters={ [ "all" ] } setTitle={ setDateTitle }
                        setFilter={ setDateFilter } currentTitle={ dateTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* media filter to filter events by whether they have media or not*/ }
                    {<EventDropDown name="media" filters={ [ "all", "has media", "no media" ] } setTitle={ setMediaTitle }
                        setFilter={ setHasMediaFilter } currentTitle={ mediaTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* quantity filter to filter events based on the number of news that are related to them*/ }
                    {<EventDropDown name="related news" filters={ [ "all", "less than 35 related news", "equal to 35 related news", "more than 35 related news" ] }
                        setTitle={ setQuantityTitle } setFilter={ setQuantity }
                        currentTitle={ quantityTitle } updatePage={ setCurrentPage } /> }
                    {/* sort filter to sort the current query by the selectable options*/ }
                    {<EventDropDown name="sort by" filters={ [ "all", "name", "promoter", "date time", "venue id" ] }
                        setTitle={ setOrderTitle } setFilter={ setOrder }
                        currentTitle={ orderTitle } updatePage={ setCurrentPage } /> }
                    {/* instantiate a search bar on the model page which searches by name */ }
                    {<EventSearchForm butSize="75" whichFilter="Search for an Event"
                        updatePage={ setCurrentPage } setTitle={ setSearch } /> }
                </Row>
            );
        }
        return ( <br></br> );
    }
    return (
        <>
            <div className="page">
                <Row className="justify-content-between modelHeader">
                    <h2>
                        <span className="modelPage">Events</span>
                        <span className="divider">&ensp;|&ensp;</span>
                        <span className="pageDeclaration">Page { currentPage }</span>
                    </h2>
                    {/* Displays the filters and search bar for the model page, depending on the searchPage boolean */ }
                    { FilterSearch( searchPage ) }
                </Row>
                <br></br><br></br>
                <Row className="no-gutter">
                    {/* Displays a row of the cardsRow list, which has up to 10 EventCards */ }
                    { cardsRow }
                </Row>
                <br></br>
                {/* Set up pagination at bottom of page */}
                {<Pagination currentWindow={currentWindow} numPages={numPages} currentPage={currentPage}
                updateWindow={setCurrentWindow} updatePage={setCurrentPage} />}
            </div>
        </>
    )
}
export default Event;
