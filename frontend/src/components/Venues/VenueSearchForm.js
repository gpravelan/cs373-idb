import { useForm } from "react-hook-form"
import React from 'react';


//SearchForm function which takes in a button size argument and a filter type argument
//outputs a search box and submit button which captures the text in the searchbox
//called by certain Dropdowns and on each model page
export function VenueSearchForm({butSize, whichFilter, updatePage, setTitle, setFilter}) {

    //useForm allows for the capture of search entries
    const{register, handleSubmit} = useForm();
    //setTitle and setFilter are callable state functions which will re render main page when changed
    const onSubmit = (data) => { //when the user hits submit button for search
        updatePage(1); //reset the current page to 1
        if(whichFilter === "Search for a Venue") {
            //model page search bar (which searches by name) update the search query for api request
            setTitle(data.search);
        } else {
            //promoter or date or venue filter, update appropriate title for dropdown and value for api request
            setTitle(data.search);
            setFilter(data.search);
        }
    }
    return (
        // displays a form and on submissions will call the onSubmit method above
        <form className="form-inline" onSubmit={ handleSubmit( onSubmit ) }>
            <input className="form-control" type="text" size={ butSize } placeholder={ whichFilter } name="search" ref={ register } />
            <input className="btn btn-outline-primary" type="submit"></input>
        </form>
    )
}

export default VenueSearchForm;
