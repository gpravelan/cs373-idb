import React from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';
import {EventData} from "./EventData"

export default function EPMLine() {

    // data preloaded for speed to avoid paginated API requests
    const unformattedData = EventData();

    let data = [];

    for (let date in unformattedData) {
        data.push({name: date, numEvents: unformattedData[date]})
    }

    return (
        <div className="page">
            <LineChart id="LineGraph"
                width={500}
                height={500}
                data={data}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Line type="monotone" dataKey="numEvents" stroke="#78003c"
                    activeDot={{ r: 8 }} />
            </LineChart>
        </div>
    )
}
