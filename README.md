# CS 373: Software Engineering: IDB Project 4


| Members | GitlabID | EID |
| ------------ | ------------- | ------------- |
| Gokula Praveen Elangovan | gpravelan | ge3959 |
| Garrett Egan | garrettegan | gwe272 |
| Seth Forman | seth4man | saf2829 |
| Luke Hill | lukeh98 | lch2299 |
| Prem Rajkitkul | JustPrem | pr23388 |

---

* Link to Website: https://findeventsnear.me
* Project Leader: Praveen
* Git SHA: 8f0cfaa8b3ecad6c5f2e2f2ad6ea2de07996f4c1
* GitLab Pipelines: https://gitlab.com/gpravelan/cs373-idb/-/pipelines

---

* Estimated completion time for each member (hours):
    - Gokula Praveen Elangovan: 5
    - Garret Egan: 3
    - Seth Forman: 3
    - Luke Hill: 4
    - Prem Rajkitkul: 5

---

* Actual completion time for each member (hours):
    - Gokula Praveen Elangovan: 9
    - Garrett Egan: 9
    - Seth Forman: 9
    - Luke Hill: 9
    - Prem Rajkitkul: 9

---

* Comments: Warnings will be logged when maps are displayed in firefox.
                According to the post below this is something that is due to
                Google's end of the api and is just dependent on the browser.
                Google Chrome does not have these warnings.

https://www.reddit.com/r/firefox/comments/fpptyj/firefox_content_security_policy_console_output/
