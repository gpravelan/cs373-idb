import React from 'react';
import { Link } from "react-router-dom";
import { Button, Container, Col, Row } from 'react-bootstrap';
import useAxios from "axios-hooks";
import ModelInformation, { ModelInformationListRow } from "../ModelInformation/ModelInformation";

//represents the complete instance of an event instance
//displays all available information rather than the key information displayed in EventCards
//clicking the More Info button on a EventCards redirects to this function
export default function EventsInstance ( { match } )
{
    //make a specific api call with the id of the news instance and store the result in data
    const [ { data, loading } ] = useAxios( `/api/event/${ match.params.id }` );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }

    return (
        <>
            <div className="page">
                {/* Each EventsInstance will access and output the name, date_time, link, promoter,
                    image, special_info, related venue, and related news links of the event */}
                <Container fluid>
                    <Row>
                        <Col className="modelName">
                            {/* Display event title */ }
                            <div className="header">{ data[ "name" ] }</div>
                            <hr></hr>
                            <Button className="backButton">
                                <Link className="backText" to={ `/events/` } >Back to Events</Link>
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="modelOverview">
                            {/* Display event media */ }
                            <img alt="" id="model-img" src={ ( data[ "media" ] ) } />
                            {/* Display event date */ }
                            { ModelInformation( "Date", data[ "date_time" ] ) }
                            {/* Display event promoter */ }
                            { ModelInformation( "Promoter", data[ "promoter" ] ) }
                            {/* Display event description */ }
                            { ModelInformation( "Description", data[ "special_info" ] ) }
                            {/* Display event link */ }
                            { ModelInformation( "Link", <a href={ data[ "link" ] }>Learn more about this event </a> ) }
                            {/* Display event venue */ }
                            { ModelInformation( "Event Venue",
                                <Link to={ `/venues/${ data[ "venue" ][ "id" ] }` }>{ data[ "venue" ][ "name" ] } </Link> ) }
                        </Col>
                        <Col className="mt-2">
                            {/* Display event's 20-most related news */ }
                            { ModelInformationListRow( "Relevant News", data[ "relevant_news" ].slice( 20 ), "news" ) }
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}
