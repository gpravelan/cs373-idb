import React from 'react';
import { Link } from "react-router-dom";
import { Button, Card, Row } from 'react-bootstrap/'
import Highlight from "../Highlight/Highlight";

//NewsCard takes in data collected from one event object after an API call and displays
//a styled card with important news information highlighted on it, including images
export function NewsCard ( { search, id, title, source, media, publication_date } )
{
    if ( publication_date )
    {
        let date_time_array = publication_date.split( "-" );
        let calendar = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
        let month = calendar[ parseInt( date_time_array[ 1 ] ) - 1 ];
        let day = date_time_array[ 2 ].split( " " )[ 0 ];
        let year = date_time_array[ 0 ];
        let time = date_time_array[ 2 ].split( " " )[ 1 ].split( ":" );
        let marker = "a";
        let hour = parseInt( time[ 0 ] )
        if ( hour > 12 )
        {
            hour -= 12;
            marker = "p"
        } else if ( hour === 0 )
        {
            hour = 12;
        }
        let minute = time[ 1 ]
        publication_date = hour + ":" + minute + marker + " " + month + " " + day + ", " + year;
    }
    //highlights any model names with the search query passed in, if applicable
    let newsName = <Highlight searchQuery = {search} word = {title} />;
    if (!media) {
        //if newsCard is not passed in an image, then renders a no media found image
        media = require("../../assets/images/noImageAvailable.jpg");
    }
    return (
        <>
            <div>
            {/* Sets up the dimensions and styling of the card */}
            <Card className="align-items-center large_card">
                <Card.Img variant="top" src= {media} />
                <Card.Body>
                    <div className="date">{ publication_date }</div>
                    <Card.Title>{newsName}</Card.Title>
                    {/* outputs the args passed in with relevant identifiers */}
                        {/* <h6>Promoter: <small>{ promoter }</small></h6> */}
                    <div className="source">{ source }</div>

                        {/* Provides a More Info button which directs to an EventsInstance which has all information */ }

                </Card.Body>
                <Row className="justify-content-center">
                    <Button disabled id={ 'button' + id } >
                        <Link to={ `/news/${ id }` } className="moreInfo">More Info</Link>
                    </Button>
                </Row>
            </Card>

            </div>
        </>
    )
}

export default NewsCard;
