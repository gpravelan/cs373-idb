import unittest
import requests
import json


class GlobVars:
    addr = "https://findeventsnear.me"


class MyTestCase(unittest.TestCase):

    ###################
    #  Phase 2 Tests  #
    ###################
    def test_api_exists(self):
        response = requests.get(GlobVars.addr + "/api/")
        self.assertIsNotNone(response)

    def test_venue_list_exists(self):
        response = requests.get(GlobVars.addr + "/api/venues")
        self.assertIsNotNone(response)

    def test_venue_list_response(self):
        response = requests.get(GlobVars.addr + "/api/venues")
        self.assertEqual(response.status_code, 200)

    def test_venue_list_data(self):
        response = requests.get(GlobVars.addr + "/api/venues?page_number=1")
        load = response.json()
        self.assertEqual(load["total_pages"], 16)
        self.assertEqual(len(load["data"]), 10)

    def test_event_list_exists(self):
        response = requests.get(GlobVars.addr + "/api/events")
        self.assertIsNotNone(response)

    def test_event_list_response(self):
        response = requests.get(GlobVars.addr + "/api/events")
        self.assertEqual(response.status_code, 200)

    def test_event_list_data(self):
        response = requests.get(GlobVars.addr + "/api/events?page_number=1")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(len(load["data"]), 10)

    def test_news_list_exists(self):
        response = requests.get(GlobVars.addr + "/api/news")
        self.assertIsNotNone(response)

    def test_news_list_response(self):
        response = requests.get(GlobVars.addr + "/api/news")
        self.assertEqual(response.status_code, 200)

    def test_news_list_data(self):
        response = requests.get(GlobVars.addr + "/api/news?page_number=1")
        load = response.json()
        self.assertEqual(load["total_pages"], 548)
        self.assertEqual(len(load["data"]), 10)

    def test_venue_single_exists(self):
        response = requests.get(GlobVars.addr + "/api/venue/1")
        self.assertIsNotNone(response)

    def test_venue_single_response(self):
        response = requests.get(GlobVars.addr + "/api/venue/1")
        self.assertEqual(response.status_code, 200)

    def test_venue_single_data(self):
        response = requests.get(GlobVars.addr + "/api/venue/1")
        self.assertEqual(len(response.json()), 9)

    def test_event_single_exists(self):
        response = requests.get(GlobVars.addr + "/api/event/4000")
        self.assertIsNotNone(response)

    def test_event_single_response(self):
        response = requests.get(GlobVars.addr + "/api/event/4000")
        self.assertEqual(response.status_code, 200)

    def test_event_single_data(self):
        response = requests.get(GlobVars.addr + "/api/event/4000")
        self.assertEqual(len(response.json()), 9)

    def test_news_single_exists(self):
        response = requests.get(GlobVars.addr + "/api/only-one-singular-piece-of-news/1")
        self.assertIsNotNone(response)

    def test_news_single_response(self):
        response = requests.get(GlobVars.addr + "/api/only-one-singular-piece-of-news/1")
        self.assertEqual(response.status_code, 200)

    def test_news_single_data(self):
        response = requests.get(GlobVars.addr + "/api/only-one-singular-piece-of-news/1")
        self.assertEqual(len(response.json()), 9)

    ###################
    #  Phase 3 Tests  #
    ###################

    def test_venue_search(self):
        response = requests.get(GlobVars.addr + "/api/venues?query=ala")
        load = response.json()
        self.assertEqual(load["total_pages"], 1)
        self.assertEqual(len(load["data"]), 2)

    def test_event_search(self):
        response = requests.get(GlobVars.addr + "/api/events?query=ramm")
        load = response.json()
        self.assertEqual(load["total_pages"], 1)
        self.assertEqual(len(load["data"]), 6)

    def test_news_search(self):
        response = requests.get(GlobVars.addr + "/api/news?query=sound")
        load = response.json()
        self.assertEqual(load["total_pages"], 1)
        self.assertEqual(len(load["data"]), 7)

    # Tests for sorting venues
    def test_venue_sort_name(self):
        response = requests.get(GlobVars.addr + "/api/venues?orderBy=name")
        load = response.json()
        self.assertEqual(load["total_pages"], 16)
        self.assertEqual(load["data"][0]["id"], 105)

    def test_venue_sort_city(self):
        response = requests.get(GlobVars.addr + "/api/venues?orderBy=city")
        load = response.json()
        self.assertEqual(load["total_pages"], 16)
        self.assertEqual(load["data"][0]["id"], 135)

    def test_venue_sort_zipcode(self):
        response = requests.get(GlobVars.addr + "/api/venues?orderBy=zipcode")
        load = response.json()
        self.assertEqual(load["total_pages"], 16)
        self.assertEqual(load["data"][0]["id"], 8)

    def test_venue_sort_hasParkingInfo(self):
        response = requests.get(GlobVars.addr + "/api/venues?orderBy=parking_info")
        load = response.json()
        self.assertEqual(load["total_pages"], 16)
        self.assertEqual(load["data"][0]["id"], 35)

    def test_venue_sort_hasAccessibleSeatingInfo(self):
        response = requests.get(GlobVars.addr + "/api/venues?orderBy=accessible_seating_info")
        load = response.json()
        self.assertEqual(load["total_pages"], 16)
        self.assertEqual(load["data"][0]["id"], 122)

    # Test sorting for events
    def test_events_sort_name(self):
        response = requests.get(GlobVars.addr + "/api/events?orderBy=name")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(load["data"][0]["id"], 5555)

    def test_events_sort_date(self):
        response = requests.get(GlobVars.addr + "/api/events?orderBy=date_time")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(load["data"][0]["id"], 3988)

    def test_events_sort_promoter(self):
        response = requests.get(GlobVars.addr + "/api/events?orderBy=promoter")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(load["data"][0]["id"], 4856)

    def test_events_sort_venue(self):
        response = requests.get(GlobVars.addr + "/api/events?orderBy=venue_id")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(load["data"][0]["id"], 5462)

    def test_events_sort_hasMedia(self):
        response = requests.get(GlobVars.addr + "/api/events?orderBy=media")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(load["data"][0]["id"], 5899)

    # Test sorting for news
    def test_news_sort_title(self):
        response = requests.get(GlobVars.addr + "/api/news?orderBy=title")
        load = response.json()
        self.assertEqual(load["total_pages"], 548)
        self.assertEqual(load["data"][0]["id"], 875)

    def test_news_sort_source(self):
        response = requests.get(GlobVars.addr + "/api/news?orderBy=source")
        load = response.json()
        self.assertEqual(load["total_pages"], 548)
        self.assertEqual(load["data"][0]["id"], 2882)

    def test_news_sort_publication_date(self):
        response = requests.get(GlobVars.addr + "/api/news?orderBy=publication_date")
        load = response.json()
        self.assertEqual(load["total_pages"], 548)
        self.assertEqual(load["data"][0]["id"], 5616)

    def test_news_sort_venue(self):
        response = requests.get(GlobVars.addr + "/api/news?orderBy=venue_id")
        load = response.json()
        self.assertEqual(load["total_pages"], 548)
        self.assertEqual(load["data"][0]["id"], 5443)

    def test_news_sort_hasMedia(self):
        response = requests.get(GlobVars.addr + "/api/news?orderBy=media")
        load = response.json()
        self.assertEqual(load["total_pages"], 548)
        self.assertEqual(load["data"][0]["id"], 291)

    # Tests for filtering venues
    def test_venue_filter_name(self):
        response = requests.get(GlobVars.addr + "/api/venues?name=alamodome")
        load = response.json()
        self.assertEqual(load["total_pages"], 1)
        self.assertEqual(load["data"][0]["id"], 1)

    def test_venue_filter_city(self):
        response = requests.get(GlobVars.addr + "/api/venues?city=Dallas")
        load = response.json()
        self.assertEqual(load["total_pages"], 1)
        self.assertEqual(load["data"][0]["id"], 5)

    def test_venue_filter_zipcode(self):
        response = requests.get(GlobVars.addr + "/api/venues?zipcode=75201")
        load = response.json()
        self.assertEqual(load["total_pages"], 1)
        self.assertEqual(load["data"][0]["id"], 5)

    def test_venue_filter_hasParkingInfo(self):
        response = requests.get(GlobVars.addr + "/api/venues?hasParkingInfo=false")
        load = response.json()
        self.assertEqual(load["total_pages"], 3)
        self.assertEqual(load["data"][0]["id"], 2)

    def test_venue_filter_hasAccessibleSeatingInfo(self):
        response = requests.get(GlobVars.addr + "/api/venues?hasAccessibleSeatingInfo=false")
        load = response.json()
        self.assertEqual(load["total_pages"], 4)
        self.assertEqual(load["data"][0]["id"], 2)

    # Test filtering for events
    def test_events_filter_name(self):
        response = requests.get(GlobVars.addr + "/api/events?name=Disney%20On%20Ice%20presents%20Dream%20Big")
        load = response.json()
        self.assertEqual(load["total_pages"], 3)
        self.assertEqual(load["data"][0]["id"], 3942)

    def test_events_filter_date(self):
        response = requests.get(GlobVars.addr + "/api/events?date=2020%2D11%2D22")
        load = response.json()
        self.assertEqual(load["total_pages"], 2)
        self.assertEqual(load["data"][0]["id"], 3942)

    def test_events_filter_promoter(self):
        response = requests.get(GlobVars.addr + "/api/events?promoter=AEG%20Live")
        load = response.json()
        self.assertEqual(load["total_pages"], 7)
        self.assertEqual(load["data"][0]["id"], 3987)

    def test_events_filter_venue(self):
        response = requests.get(GlobVars.addr + "/api/events?venue=Alamodome")
        load = response.json()
        self.assertEqual(load["total_pages"], 2)
        self.assertEqual(load["data"][0]["id"], 3942)

    def test_events_filter_hasMedia(self):
        response = requests.get(GlobVars.addr + "/api/events?hasMedia=true")
        load = response.json()
        self.assertEqual(load["total_pages"], 210)
        self.assertEqual(load["data"][0]["id"], 3942)

    # Test filtering for news
    def test_news_filter_source(self):
        response = requests.get(GlobVars.addr + "/api/news?source=yahoo%2Ecom")
        load = response.json()
        self.assertEqual(load["total_pages"], 4)
        self.assertEqual(load["data"][0]["id"], 150)

    def test_news_filter_publication_date(self):
        response = requests.get(GlobVars.addr + "/api/news?publicationDate=2020%2D10%2D13")
        load = response.json()
        self.assertEqual(load["total_pages"], 69)
        self.assertEqual(load["data"][0]["id"], 8)

    def test_news_filter_venue(self):
        response = requests.get(GlobVars.addr + "/api/news?venue=alamodome")
        load = response.json()
        self.assertEqual(load["total_pages"], 4)
        self.assertEqual(load["data"][0]["id"], 1)

    def test_news_filter_hasMedia(self):
        response = requests.get(GlobVars.addr + "/api/news?hasMedia=false")
        load = response.json()
        self.assertEqual(load["total_pages"], 100)
        self.assertEqual(load["data"][0]["id"], 11)

if __name__ == '__main__':
    unittest.main()
