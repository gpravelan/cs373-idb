import React from 'react';
import {Cell, PieChart, Pie, Tooltip} from "recharts";
import Legend from "recharts/lib/component/Legend";

export default function APTPie() {

    // data preloaded for speed to avoid paginated API requests
    const data = [
        { name: 'Morning', value: 1046 },
        { name: 'Afternoon', value: 2438 },
        { name: 'Evening', value: 1990 }
    ];

    const COLORS = ['#c89664', '#3c5a78', '#78003c']

    return (
        <div className="page">
            <PieChart width={500} height={500}>
                <Pie dataKey="value" data={data} cx={250} cy={150}
                        outerRadius={150} fill={'#c89664'} >
                    {data.map((entry, index) =>
                                <Cell key={"pieKey" + index}
                                fill={COLORS[index % COLORS.length]}/>)}
                </Pie>
                <Tooltip />
                <Legend iconSize={50}/>
            </PieChart>
        </div>
    )
}
