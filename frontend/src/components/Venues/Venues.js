import React, { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap/'
import useAxios from 'axios-hooks';
import VenueCard from './VenueCard'
import VenueDropDown from './VenueDropDown'
import VenueSearchForm from './VenueSearchForm'
import Pagination from "../Pagination/Pagination"
import "../../assets/styles/Pagination.css";
import "../../assets/styles/ModelViewer.css"


export const Venues = ( props ) =>
{
    let defaultSearch = 'none';
    let searchPage = false;
    //if Venues was called with a search query passed in, we are on the searchpage
    //this will affect whether filters and model search bars are displayed, and whether
    //the initial search is empty or instantiated to the query passed in
    if ( props.query )
    {
        defaultSearch = props.query;
        searchPage = true;
    }

    // set up search state using useState and useEffect to capture any changes
    const [ search, setSearch ] = useState( defaultSearch );
    useEffect( () => { setSearch( defaultSearch ) }, [ defaultSearch ] );

    //pagination states to keep track of current page and current page window
    const [ currentPage, setCurrentPage ] = useState( 1 );
    const [ currentWindow, setCurrentWindow ] = useState( 0 );

    //search, sort filter variables which represent values to be passed into the api call.
    //these variables all useState such that when one is changed the page is re rendered
    const [ cityFilter, setCityFilter ] = useState( 'none' );
    const [ zipCodeFilter, setZipCodeFilter ] = useState( 'none' );
    const [ hasParkingFilter, setHasParkingFilter ] = useState( 'none' );
    const [ hasSeatingFilter, setHasSeatingFilter ] = useState( 'none' );
    const [ order, setOrder ] = useState( 'none' );

    //search, sort, filter title states which update to represent which filters are being used
    //on the current page; title states are displayed on dropdowns
    const [ cityTitle, setCityTitle ] = useState( 'city' );
    const [ zipCodeTitle, setZipCodeTitle ] = useState( 'zip code' );
    const [ parkingTitle, setParkingTitle ] = useState( 'parking info' );
    const [ seatingTitle, setSeatingTitle ] = useState( 'seating info' );
    const [ orderTitle, setOrderTitle ] = useState( 'sort by' );

    //scan through the search sort filter variables and append to API request variable
    //appropriately based on the values of the different filter variables
    let request = '';
    ( cityFilter !== 'none' ) ? request += `&city=${ cityFilter }` : request += '';
    ( search !== 'none' && search !== '' ) ? request += `&query=${ search }` : request += '';
    ( zipCodeFilter !== 'none' ) ? request += `&zipcode=${ zipCodeFilter }` : request += '';
    ( hasParkingFilter !== 'none' ) ? request +=
        `&hasParkingInfo=${ hasParkingFilter }` : request += '';
    ( hasSeatingFilter !== 'none' ) ? request +=
        `&hasAccessibleSeatingInfo=${ hasSeatingFilter }` : request += '';
    ( order !== 'none' ) ? request += `&orderBy=${ order }` : request += '';

    //make api call by appending request, which is updated dynamically with useState
    const [ { data, loading } ] = useAxios( `/api/venues?page_number=${ currentPage }` + request );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }
    // grab the data variable which has api call results and the number of pages
    const pagesData = data.data;
    const numPages = data.total_pages;

    let cardsRow = [];
    for ( let i = 0; i < pagesData.length; i++ )
    {
        //cardsRow is a list of up to 10 cards (length of pagesData) which store all information
        //regarding a venue object/model
        cardsRow.push(
            <Col key={ pagesData[ i ][ "id" ] }>
                <VenueCard search={ search } id={ pagesData[ i ][ "id" ] }
                    name={ pagesData[ i ][ "name" ] } address={ pagesData[ i ][ "address" ] }
                    city={ pagesData[ i ][ "city" ] } zipcode={ pagesData[ i ][ "zip_code" ] }
                    parking_info={ pagesData[ i ][ "parking_info" ] }
                    accessible_seating_info={ pagesData[ i ][ "accessible_seating_info" ] } />
            </Col> )
    }

    //structure which holds the filters and search bar for each model page; outputs them in one row
    //takes in a bool searchPage; if its true we are on the searchpage and don't need filters/search bar
    function FilterSearch ( searchPage )
    {
        if ( !searchPage )
        {
            return (
                <Row>
                    {/* city filter with popular suggestions and a search bar for any user specified filter*/ }
                    {<VenueDropDown a='Search for a City' name="city"
                        color="success" filters={ [ "all", "New York", "Dallas", "Las Vegas",
                            "Las Angeles", "Philadelphia", "San Antonio" ] } setTitle={ setCityTitle }
                        setFilter={ setCityFilter } currentTitle={ cityTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* zip code filter with popular suggestions and a search bar for any user specified filter*/ }
                    {<VenueDropDown a='Search for a Zip Code' name="zip code"
                        filters={ [ "all" ] } setTitle={ setZipCodeTitle }
                        setFilter={ setZipCodeFilter } currentTitle={ zipCodeTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* parking info filter with dropdown options*/ }
                    {<VenueDropDown name="parking info" color="warning"
                        filters={ [ "all", "has parking info", "no parking info" ] }
                        setTitle={ setParkingTitle } setFilter={ setHasParkingFilter }
                        currentTitle={ parkingTitle } updatePage={ setCurrentPage } /> }
                    {/* seating info filter with dropdown options*/ }
                    {<VenueDropDown name="seating info" color="danger"
                        filters={ [ "all", "has seating info", "no seating info" ] }
                        setTitle={ setSeatingTitle } setFilter={ setHasSeatingFilter }
                        currentTitle={ seatingTitle } updatePage={ setCurrentPage } /> }
                    {/* sort filter to sort the current query by the selectable options*/ }
                    {<VenueDropDown name="sort by" color="secondary"
                        filters={ [ "all", "name", "city", "zipcode", "parking info", "accessible seating info" ] }
                        setTitle={ setOrderTitle } setFilter={ setOrder }
                        currentTitle={ orderTitle } updatePage={ setCurrentPage } /> }
                    {/* instantiate a search bar on the model page which searches by name */ }
                    {<VenueSearchForm butSize="75" whichFilter="Search for a Venue"
                        updatePage={ setCurrentPage } setTitle={ setSearch } /> }
                </Row>
            );
        }
        return ( <br></br> );
    }

    return (
        <>
            <div className="page">
                <Row className="justify-content-between modelHeader">
                    <h2>
                        <span className="modelPage">Venues</span>
                        <span className="divider">&ensp;|&ensp;</span>
                        <span className="pageDeclaration">Page { currentPage }</span>
                    </h2>
                    {/* Displays the filters and search bar for the model page, depending on the searchPage boolean */ }
                    { FilterSearch( searchPage ) }
                </Row>
                <br></br><br></br>
                <Row className="no-gutter">
                    {/* Displays a row of the cardsRow list, which has up to 10 VenueCards */ }
                    { cardsRow }
                </Row>
                {/* Set up pagination at bottom of page */}
                {<Pagination currentWindow={currentWindow} numPages={numPages} currentPage={currentPage}
                updateWindow={setCurrentWindow} updatePage={setCurrentPage} />}
            </div>
        </>
    )
}

export default Venues;
