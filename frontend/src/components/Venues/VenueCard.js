import React from 'react';
import { Link } from "react-router-dom";
import { Button, Card, Row } from 'react-bootstrap/'
import Highlight from "../Highlight/Highlight";
import Map from "../Map/Map";

//VenueCard takes in data collected from one venue object after an API call and displays
//a styled card with important venue information highlighted on it
export function VenueCard ({search, id, name, address, city, zipcode}) {
    //highlights any model names with the search query passed in, if applicable
    let venuesName = <Highlight searchQuery = {search} word = {name} />;
    return (
        <>
            <div>
            {/* Sets up the dimensions and styling of the card */}
            <Card className="align-items-center">
                {Map(address, city)}
                <Card.Body>
                    <Card.Title className="mb-3 ">{venuesName}</Card.Title>
                    {/* outputs the args passed in with relevant identifiers */}
                        <h6>
                            <span className="field">Address: </span>
                            <small>{ address }</small>
                        </h6>
                        <h6>
                            <span className="field">City: </span>
                            <small>{ city }</small>
                        </h6>
                        <h6>
                            <span className="field">Zipcode: </span>
                            <small>{ zipcode }</small>
                        </h6>
                    {/* Provides a More Info button which directs to an EventsInstance which has all information */}
                        <Row className="justify-content-center">
                            <Button disabled id={ 'button' + id } >
                                <Link to={ `/venues/${ id }` } className="moreInfo">More Info</Link>
                            </Button>
                        </Row>
                </Card.Body>
            </Card>

            </div>
        </>
    )
}

export default VenueCard;
