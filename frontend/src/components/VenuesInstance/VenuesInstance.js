import React from 'react';
import { Link } from "react-router-dom";
import { Button, Container, Col, Row } from 'react-bootstrap';
import useAxios from "axios-hooks";
import ModelInformation, { ModelInformationListRow } from "../ModelInformation/ModelInformation";
import Map from "../Map/Map";
import "../../assets/styles/ModelInstance.css"

//represents the complete instance of a venue instance
//displays all available information rather than the key information displayed in VenueCards
//clicking the More Info button on a VenueCard redirects to this function
export default function VenuesInstance ( { match } )
{
    //make a specific api call with the id of the news instance and store the result in data
    const [ { data, loading } ] = useAxios( `/api/venue/${ match.params.id }` );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }

    return (
        <>
            <div className="page">
                {/* Each VenuesInstance will access and output the name, address, city, zip code,
                    parking info, image, seating info, related news link, and
                    related event links of the venue */}
                <Container fluid>
                    <Row>
                        <Col className="modelName">
                            {/* Display venue title */ }
                            <div className="header">{ data[ "name" ] }</div>
                            <hr></hr>
                            <Button className="backButton">
                                <Link className="backText" to={ `/venues/` } >Back to Venues</Link>
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="modelOverview">
                            {/* Display venue media */ }
                            { Map( data[ "address" ], data[ "city" ] ) }
                            {/* Display venue location */ }
                            { ModelInformation( "Location", data[ "address" ] + ", " + data[ "city" ] + " " + data[ "zip_code" ] ) }
                            {/* Display venue parking information */ }
                            { ModelInformation( "Parking", data[ "parking_info" ] ) }
                            {/* Display venue accessible seating information */ }
                            { ModelInformation( "Accessibility", data[ "accessible_seating_info" ] ) }
                        </Col>
                        <Col className="mt-2">
                            {/* Display venue's 10 most related events */ }
                            { ModelInformationListRow( "Relevant Events", data[ "relevant_events" ].slice( 0, 10 ), "events" ) }
                            {/* Display venue's 10 most related news */ }
                            { ModelInformationListRow( "Relevant News", data[ "relevant_news" ].slice( 0, 10 ), "news" ) }
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}
