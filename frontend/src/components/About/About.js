import React from 'react';
import { Row, Col } from 'react-bootstrap/';
import { AiOutlineGitlab, AiOutlineApi } from "react-icons/ai";
import { ImTicket } from "react-icons/im";
import { GiFishingNet, GiNewspaper } from "react-icons/gi";
import { MdMap } from "react-icons/md";
import '../../assets/styles/About.css';
import getStats, {parseCommitData, parseIssueData} from './GitlabStats.js'
import {AboutStrings} from "./AboutStrings.js"
import DevCardRow from './DevCards.js'
import ToolsList from './Tools.js'

const aboutStrings = AboutStrings();


const tools = [ ['AWS', aboutStrings[ 'aws' ]],
                ['Gitlab', aboutStrings[ 'gitlab' ]],
                ['Postman', aboutStrings[ 'postman' ]],
                ['React', aboutStrings[ 'react' ]],
                ['React Bootstrap', aboutStrings[ 'react-bootstrap' ]],
                ['MySQL', aboutStrings[ 'mysql' ]],
                ['Flask', aboutStrings[ 'flask' ]],
                ['Slack', aboutStrings[ 'slack' ]],
                ['Jest', aboutStrings[ 'slack' ]],
                ['Docker', aboutStrings[ 'docker' ]],
                ['Namecheap', aboutStrings[ 'namecheap' ]],
                ['Selenium', aboutStrings[ 'selenium' ]] ];

const developers = [ [aboutStrings["sethName"],
                        aboutStrings["sethImage"],
                        aboutStrings["sethAbt"],
                        aboutStrings["sethAuthorName"],
                        aboutStrings["sethUsername"],
                        66,
                        aboutStrings["sethLinkedIn"]],
                    [aboutStrings["garrettName"],
                        aboutStrings["garrettImage"],
                        aboutStrings["garrettAbt"],
                        aboutStrings["garrettAuthorName"],
                        aboutStrings["garrettUsername"],
                        0,
                        aboutStrings["garrettLinkedIn"]],
                     [aboutStrings["praveenName"],
                        aboutStrings["praveenImage"],
                        aboutStrings["praveenAbt"],
                        aboutStrings["praveenAuthorName"],
                        aboutStrings["praveenUsername"],
                        0,
                        aboutStrings["praveenLinkedIn"]],
                     [aboutStrings["lukeName"],
                        aboutStrings["lukeImage"],
                        aboutStrings["lukeAbt"],
                        aboutStrings["lukeAuthorName"],
                        aboutStrings["lukeUsername"],
                        29,
                        aboutStrings["lukeLinkedIn"]],
                     [aboutStrings["premName"],
                        aboutStrings["premImage"],
                        aboutStrings["premAbt"],
                        aboutStrings["premAuthorName"],
                        aboutStrings["premUsername"],
                        0,
                        aboutStrings["premLinkedIn"]]
                      ]

const COMMITS = "https://gitlab.com/api/v4/projects/21430345/repository/commits"
const ISSUES = "https://gitlab.com/api/v4/projects/21430345/issues";

function updateCommitData(data){
    let statsToUpdate = document.getElementsByClassName("commitStat");
    for(let i = 0; i < statsToUpdate.length; i++){
        const element = statsToUpdate[i];
        element.innerHTML = data[element.id];
    }
}

function updateIssueData(data){
    let statsToUpdate = document.getElementsByClassName("issueStat");
    for(let i = 0; i < statsToUpdate.length; i++){
        const element = statsToUpdate[i];
        element.innerHTML = data[element.id];
    }
}

export const About = () => {
    getStats(COMMITS).then((data) => updateCommitData(parseCommitData(data)));
    getStats(ISSUES).then((data) => updateIssueData(parseIssueData(data)));

    return(
        <div className='page'>
            <Row>
                <Col id="scrollable">
                    <Row>
                    <h2 className="stats-header">&ensp;
                        <span className="about-highlight">
                            Who We Are:&ensp;&emsp;&emsp;&ensp;
                        </span>
                        {StatTotal('commitStat', 0, ' Commits')}
                        {StatTotal('issueStat', 0, ' Issues')}
                        {StatTotal('', 95, ' Tests')}
                    </h2>
                    </Row>
                    {DevCardRow(developers)}
                </Col>
                <Col>
                    <div>
                    <h2 className="about-header">About</h2>
                    <h4 className="about-text">{aboutStrings["whyOne"]}
                        <br></br><br></br>
                        {aboutStrings["whyTwo"]}
                    </h4>
                    <Row className="justify-content-center">
                    <h2 className="about-header-accent">
                        <AiOutlineGitlab className="about-highlight"/>
                        <a className="about-header-accent"
                            href= {aboutStrings['gitlabUrl']}>
                            <span className="underline">Gitlab</span>
                        &emsp;</a>
                        <AiOutlineApi className="about-highlight"/>
                        <a className="about-header-accent"
                            href= {aboutStrings['postmanUrl']}>
                            <span className="underline">Postman</span>
                        &emsp;</a>
                    </h2>
                    </Row>
                    </div>
                    <div>
                    <h2 className="about-header">Data Sources and Tools</h2>
                    {ToolsList(tools)}

                    <Row className="justify-content-center">
                        <h4 className="about-header-accent">
                        <GiFishingNet className="about-highlight"/>
                        <a className="about-header-accent" href={aboutStrings['ncUrl']}>
                            <span className="underline">Newscatcher</span>
                        &nbsp;&emsp;&emsp;</a>
                        <GiNewspaper className="about-highlight"/>
                        <a className="about-header-accent" href={aboutStrings['nApiUrl']}>
                            <span className="underline">News API</span>
                        &nbsp;&emsp;&emsp;</a>
                        <MdMap className="about-highlight"/>
                        <a className="about-header-accent" href={aboutStrings['gmUrl']}>
                            <span className="underline">Google Maps Embed</span>
                        &nbsp;&emsp;&emsp;</a>
                        <ImTicket className="about-highlight"/>
                        <a className="about-header-accent" href={aboutStrings['tmUrl']}>
                            <span className="underline">TicketMaster</span>
                        </a>
                    </h4>
                    </Row>
                    <br></br><br></br>
                    </div>
                </Col>
            </Row>
        </div>
    );
}

function StatTotal(className, defaultValue, text) {
    return (
        <>
            <span className={"about-highlight-accent " + className} id="total">
                {defaultValue}
            </span>
            <span className="about-highlight-secondary-accent">
                {text}&ensp;
            </span>
        </>
    );
}

export default About;
