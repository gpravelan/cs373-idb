import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Container, Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import '../../assets/styles/Navigation.css';
class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleEnter = this.handleEnter.bind(this);

}
  handleChange = (e) => {
    this.setState({ query: e.target.value.trim() });
  };

  handleEnter = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      this.btn.click();
    }
  }

  render() {
    return (
      <>
        <Container>
          <Navbar id="appbar" fixed="top" >
            <Navbar.Brand id="brand" href="/">Find Events Near Me</Navbar.Brand>
            <Nav className="ml-auto mr-5">
              <Nav.Link as={NavLink} to="/" exact activeClassName="active">Home</Nav.Link>
              <Nav.Link as={NavLink} to="/about" activeClassName="active">About</Nav.Link>
              <Nav.Link as={NavLink} to="/events" activeClassName="active">Events</Nav.Link>
              <Nav.Link as={NavLink} to="/news" activeClassName="active">News</Nav.Link>
              <Nav.Link as={ NavLink } to="/venues" activeClassName="active">Venues</Nav.Link>
              <Nav.Link as={ NavLink } to="/visualizations" activeClassName="active">Visualizations</Nav.Link>
            </Nav>
            <Form inline >
              <FormControl type="text" placeholder="Looking for something?" className="px-4 ml-sm-5 mr-sm-3" onChange={this.handleChange} onKeyPress={this.handleEnter}/>
              <Button as={NavLink} id="search" to={{pathname: "/search", state: {searchQuery: this.state.query}}} activeClassName="active" ref={node => (this.btn = node)} type="submit" variant="outline-primary">Search</Button>
            </Form>
          </Navbar>
        </Container>
      </>
    );
  }
}

export default Navigation;
