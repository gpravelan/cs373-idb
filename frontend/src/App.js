import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './assets/styles/App.css';
import "./assets/styles/Loading.css"
import Navigation from './components/Navigation/Navigation';
import Search from "./components/Search/Search";


import About from "./components/About/About";
import Events from "./components/Events/Events";
import Home from "./components/Home/Home"
import News from "./components/News/News"
import Venues from "./components/Venues/Venues"
import Visualizations from "./components/Visualizations/Visualizations"
import FoodsBar from "./components/ProviderVisualizations/FoodsBar"
import ConditionsScatterPlot from "./components/ProviderVisualizations/ConditionsScatterPlot"

import EventsInstance from './components/EventsInstance/EventsInstance';
import NewsInstance from './components/NewsInstance/NewsInstance';
import VenuesInstance from './components/VenuesInstance/VenuesInstance';

class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <Navigation/>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route exact path="/about" component={About} />
            <Route exact path="/events" component={Events} />
            <Route exact path="/news" component={News} />
            <Route exact path="/venues" component={ Venues } />
            <Route exact path="/visualizations" component={ Visualizations } />
            <Route exact path="/providerBar" component={ FoodsBar } />
            <Route exact path="/providerScatter" component={ ConditionsScatterPlot } />

            <Route path="/events/:id" component={EventsInstance} />
            <Route path="/news/:id" component={NewsInstance} />
            <Route path="/venues/:id" component={VenuesInstance} />
            <Route exact path="/search" component={Search} />
          </Switch>
          </Router>
      </div>
    );
  }
};

export default App;
