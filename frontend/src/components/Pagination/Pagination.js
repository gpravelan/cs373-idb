import React from 'react';
import { Button, Row } from 'react-bootstrap/'

//Pagination component used for model view pages
//Takes in state variables currentWindow and currentPage to determine next page value
//Takes in state functions updateWindow and updatePage to update page and re render
export function Pagination ({currentWindow, numPages, currentPage, updateWindow, updatePage}) {
    //pagination, stored in list represented by pageNumbers variable
    
    const pageNumbers = [];
    const pagesPerWindow = 10; //upper limit of 10 pages per page window
    const pageStart = currentWindow * pagesPerWindow; //get the current window of pages
    const pageLimit = Math.min( pagesPerWindow, numPages - pageStart ) //last page window may have < 10 pages
    for ( let i = pageStart + 1; i <= pageStart + pageLimit && i <= numPages; i++ )
    {
        pageNumbers.push( parseInt( i ) );
    }
    return (
        <Row className="justify-content-center">
            <nav className="pageNav">
                {/* Displays interactive pagination using the pagination states */ }
                <ul className='pagination'>
                    {/* Display prev page and prev page window buttons */ }
                    { <Button className="primaryPaginate" onClick={ () =>
                        updateWindow( Math.max( 0, currentWindow - 1 ) ) }>{ '<<' }</Button> }
                    { <Button className="primaryPaginate" onClick={ () =>
                        updatePage( Math.max( currentPage - 1, 1 ) ) }>{ 'prev' }</Button> }
                    {/* Displays a list of up to windowSize page buttons, which on
            click will set the current page and re render API calling if changed */}
                    { pageNumbers.map( number => (
                        <li key={ number } className='page-item'
                            id={ 'page-item ' + number }>
                            <Button onClick={ () => updatePage( number ) } className='page-link'>
                                { number }
                            </Button>
                        </li>
                    ) ) }
                    {/* Display next page and next pageWindow buttons */ }
                    { <Button className="primaryPaginate" onClick={ () =>
                        updatePage( Math.max( 1, Math.min( currentPage + 1, numPages ) ) ) }>{ 'next' }</Button> }
                    { <Button className="primaryPaginate" onClick={ () =>
                        updateWindow( parseInt( Math.min( currentWindow + 1,
                            ( numPages / pagesPerWindow ) ) ) ) }>{ '>>' }</Button> }
                </ul>
            </nav>
        </Row>
    )
}

export default Pagination;