import React from "react";
import ReactDOM from "react-dom";
import "./assets/styles/index.css";
import Favicon from 'react-favicon';
import App from "./App";

ReactDOM.render(
    <div>
        <App />
        <Favicon url={"https://cdn.discordapp.com/attachments/" +
      "395067191751344128/780583556660461608/favicon.ico"}/>

    </div>,
    document.getElementById("root")
);
