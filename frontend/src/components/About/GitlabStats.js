
// Gets a page worth of stats data from the
// provided url
async function getPage(url, page = 1) {
    let urlWithParams = url + "?page=" + page;
    var apiResults = await fetch(urlWithParams)
    .then(resp=>{
    return resp.json();
    });

    return apiResults;
}

// Gets all the stats from the provided stat url
export default async function getStats(url) {
    let page = 1
    let data = [];
    let results = await getPage(url, page);
    while (results.length > 0) {
        data.push(results);
        results = await getPage(url, ++page);
    }
    return data
};

// Parses the data of commit stats and return the
// data in a dictionary along with the total
// number of commits
export function parseCommitData(data) {
    let commitStats = {};

    let totalCommits = 0;
    for (var pageIndex in data) {
        for (var index in data[pageIndex]){
            let commit = data[pageIndex][index];
            if(commit.author_name === 'Luke Hill')
                commit.author_name = 'lukeh98';
            if(!(commit.author_name in commitStats))
                commitStats[commit.author_name] = 0;
            ++commitStats[commit.author_name];
            ++totalCommits;
        }
    }

    commitStats['total'] = totalCommits;
    return commitStats;

}

// Parses the data of issue stats and return the
// data in a dictionary along with the total
// number of issues
export function parseIssueData(data) {
    let issueStats = {};

    let totalClosed = 0;
    for (var pageIndex in data) {
        for (var index in data[pageIndex]){
            let issue = data[pageIndex][index];
            if(issue.closed_by){
                if(!(issue.closed_by.username in issueStats))
                    issueStats[issue.closed_by.username] = 0;
                ++issueStats[issue.closed_by.username];
                ++totalClosed;
            }
        }
    }

    issueStats['total'] = totalClosed;
    return issueStats;
}
