from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#find website title
def unittest1(driver):
    if driver.title == "Find Events Near Me" :
        print("Reached Splash Page")
    else:
        print("Cannot Reach Site")


#navigate to about page
def unittest2(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/about"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        print("Reached About Page")
    except:
        print("Failed to Reach About Page")

    #able to get full text of about page, commented out to keep output clean
    # about = driver.find_element_by_class_name("container-fluid")
    # print(about.text)


#navigate to events page
def unittest3(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/events"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        print("Reached Events Page")
    except:
        print("Failed to Reach Events Page")


#test pagination
def unittest4(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "page-item 2"))
        )
        page = driver.find_element_by_id("page-item 2")
        page.click()
        print("Clicked Event Pagination")
    except:
        print("Failed to Reach Events Pagination")


#test instances
def unittest5(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "button3952"))
        )
        page = driver.find_element_by_id("button3952")
        page.click()
        print("Clicked Event Instance")
    except:
        print("Failed to Reach Events Instance")


#test search bar
def unittest6(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/events"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
    except:
        print("Failed to Reach Events Page")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, "search"))
        )
        search = driver.find_element_by_name("search")
        search.send_keys("Rammstein")
        search.send_keys(Keys.RETURN)
        print("Used Events Search")
    except:
        print("Failed to use Events Search")


#navigate to news page
def unittest7(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/news"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        print("Reached News Page")
    except:
        print("Failed to Reach News Page")


#test pagination
def unittest8(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "page-item 2"))
        )
        page = driver.find_element_by_id("page-item 2")
        page.click()
        print("Clicked News Pagination")
    except:
        print("Failed to Reach News Pagination")


#test instances
def unittest9(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "button18"))
        )
        page = driver.find_element_by_id("button18")
        page.click()
        print("Clicked News Instance")
    except:
        print("Failed to Reach News Instance")


#test search bar
def unittest10(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/news"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
    except:
        print("Failed to Reach News Page")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, "search"))
        )
        search = driver.find_element_by_name("search")
        search.send_keys("Election")
        search.send_keys(Keys.RETURN)
        print("Used News Search")
    except:
        print("Failed to use News Search")


#navigate to venues page
def unittest11(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/venues"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        print("Reached Venues Page")
    except:
        print("Failed to Reach Venues Page")


#test pagination
def unittest12(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "page-item 2"))
        )
        page = driver.find_element_by_id("page-item 2")
        page.click()
        print("Clicked Event Pagination")
    except:
        print("Failed to Reach Events Pagination")


#test instances
def unittest13(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "button11"))
        )
        page = driver.find_element_by_id("button11")
        page.click()
        print("Clicked Venues Instance")
    except:
        print("Failed to Reach Venues Instance")


#test search bar
def unittest14(driver):
    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
        nav = driver.find_element_by_xpath('//a[@href="/venues"]')
        nav.click()
    except:
        print("Failed to Load NavBar")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "root"))
        )
    except:
        print("Failed to Reach Venues Page")

    try:
        main = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, "search"))
        )
        search = driver.find_element_by_name("search")
        search.send_keys("Minute Maid")
        search.send_keys(Keys.RETURN)
        print("Used Venues Search")
    except:
        print("Failed to use Venues Search")

    

def run_unit_tests ():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--window-size=1420,1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(chrome_options=chrome_options)


    #connect driver to our splash page
    driver.get("https://www.findeventsnear.me")
    unittest1(driver)
    unittest2(driver)
    unittest3(driver)
    unittest4(driver)
    unittest5(driver)
    unittest6(driver)
    unittest7(driver)
    unittest8(driver)
    unittest9(driver)
    unittest10(driver)
    unittest11(driver)
    unittest12(driver)
    unittest13(driver)
    unittest14(driver)
    
    driver.quit()


if __name__ == '__main__':
    run_unit_tests()
