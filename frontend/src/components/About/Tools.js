import React from 'react';

function Tool(name, description) {
    return (
        <li className="remove-list-bullet">
            <span className="about-highlight">{name}: </span> {description}
        </li>
    );
}

export default function ToolsList(tools){

    return (
        <ul className="about-text">
              {tools.map(listitem => (
                  Tool(listitem[0], listitem[1])
              ))}
        </ul>
    );
}
