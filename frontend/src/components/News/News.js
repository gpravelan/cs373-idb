import React, { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap/'
import useAxios from 'axios-hooks'
import NewsCard from './NewsCard'
import NewsDropDown from './NewsDropDown'
import NewsSearchForm from './NewsSearchForm'
import Pagination from "../Pagination/Pagination"
import "../../assets/styles/Pagination.css";
import "../../assets/styles/ModelViewer.css"


export const News = ( props ) =>
{
    let defaultSearch = 'none';
    let searchPage = false;
    //if News was called with a search query passed in, we are on the searchpage
    //this will affect whether filters and model search bars are displayed, and whether
    //the initial search is empty or instantiated to the query passed in
    if ( props.query )
    {
        defaultSearch = props.query;
        searchPage = true;
    }
    // set up search state using useState and useEffect to capture any changes
    const [ search, setSearch ] = useState( defaultSearch );
    useEffect( () => { setSearch( defaultSearch ) }, [ defaultSearch ] );

    //pagination states to keep track of current page and current page window
    const [ currentPage, setCurrentPage ] = useState( 1 );
    const [ currentWindow, setCurrentWindow ] = useState( 0 );

    //search, sort filter variables which represent values to be passed into the api call.
    //these variables all useState such that when one is changed the page is re rendered
    const [ sourceFilter, setSourceFilter ] = useState( 'none' );
    const [ venueFilter, setVenueFilter ] = useState( 'none' );
    const [ hasMediaFilter, setHasMediaFilter ] = useState( 'none' );
    const [ order, setOrder ] = useState( 'none' );
    const [ quantity, setQuantity ] = useState( 'none' );
    const [ dateFilter, setDateFilter ] = useState( 'none' );

    //search, sort, filter title states which update to represent which filters are being used
    //on the current page; title states are displayed on dropdowns
    const [ sourceTitle, setSourceTitle ] = useState( 'source' );
    const [ venueTitle, setVenueTitle ] = useState( 'venue' );
    const [ mediaTitle, setMediaTitle ] = useState( 'media' );
    const [ orderTitle, setOrderTitle ] = useState( 'sort by' );
    const [ quantityTitle, setQuantityTitle ] = useState( 'related events' );
    const [ dateTitle, setDateTitle ] = useState( 'publication date' );

    //scan through the search sort filter variables and append to API request variable
    //appropriately based on the values of the different filter variables
    let request = '';
    ( search !== 'none' && search !== '' ) ? request += `&query=${ search }` : request += '';
    ( venueFilter !== 'none' ) ? request += `&venue=${ venueFilter }` : request += '';
    ( sourceFilter !== 'none' ) ? request += `&source=${ sourceFilter }` : request += '';
    ( hasMediaFilter !== 'none' ) ? request += `&hasMedia=${ hasMediaFilter }` : request += '';
    ( order !== 'none' ) ? request += `&orderBy=${ order }` : request += '';
    ( quantity !== 'none' ) ? request += `&eventsQuantity${ quantity }` : request += '';
    ( dateFilter !== 'none' ) ? request += `&publicationDate=${ dateFilter }` : request += '';

    //make api call by appending request, which is updated dynamically with useState
    const [ { data, loading } ] = useAxios( `/api/news?page_number=${ currentPage }` + request );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }
    // grab the data variable which has api call results and the number of pages
    const pagesData = data.data;
    const numPages = data.total_pages;
    let cardsRow = [];
    for ( let i = 0; i < pagesData.length; i++ )
    {
        //cardsRow is a list of up to 10 cards (length of pagesData) which store all information
        //regarding a news object/model
        cardsRow.push(
            <Col key={ pagesData[ i ][ "id" ] }>
                <NewsCard search={ search } id={ pagesData[ i ][ "id" ] }
                    title={ pagesData[ i ][ "title" ] } source={ pagesData[ i ][ "source" ] }
                    link={ pagesData[ i ][ "link" ] } media={ pagesData[ i ][ "media" ] }
                    summary={ pagesData[ i ][ "summary" ] }
                    publication_date={ pagesData[ i ][ "publication_date" ] } />
            </Col> )
    }
    //structure which holds the filters and search bar for each model page; outputs them in one row
    //takes in a bool searchPage; if its true we are on the searchpage and don't need filters/search bar
    function filterSearch ( searchPage )
    {
        if ( !searchPage )
        {
            return (
                <Row>
                    {/* source filter with popular suggestions and a search bar for any user specified filter*/ }
                    {<NewsDropDown a='Search for a Source' name="source"
                        color="success" filters={ [ "all", "6abc.com", "Yahoo.com", "Stamfordadvocate.com",
                            "Nypost.com", "nbcsports.com" ] } setTitle={ setSourceTitle }
                        setFilter={ setSourceFilter } currentTitle={ sourceTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* venue filter with popular suggestions and a search bar for any user specified filter*/ }
                    {<NewsDropDown a='Search for a Venue' name="venue"
                        filters={ [ "all", "Alamodome", "Madison Square Garden", "The Met Philadelphia",
                            "United Center", "CHI Health Center Omaha" ] } setTitle={ setVenueTitle }
                        setFilter={ setVenueFilter } currentTitle={ venueTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* media filter to filter events by whether they have media or not*/ }
                    {<NewsDropDown name="media" color="warning"
                        filters={ [ "all", "has media", "no media" ] } setTitle={ setMediaTitle }
                        setFilter={ setHasMediaFilter } currentTitle={ mediaTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* date filter with a search bar for any user specified filter*/ }
                    {<NewsDropDown a='YYYY-MM-DD' name="publication date"
                        color="success" filters={ [ "all" ] } setTitle={ setDateTitle }
                        setFilter={ setDateFilter } currentTitle={ dateTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* quantity filter to filter news based on the number of events that are related to them*/ }
                    {<NewsDropDown name="related events"
                        color="danger" filters={ [ "all", "less than 5 related events", "equal to 5 related events",
                            "more than 5 related events" ] } setTitle={ setQuantityTitle }
                        setFilter={ setQuantity } currentTitle={ quantityTitle }
                        updatePage={ setCurrentPage } /> }
                    {/* sort filter to sort the current query by the selectable options*/ }
                    {<NewsDropDown name="sort by" color="secondary"
                        filters={ [ "all", "title", "source", "publication date", "venue id" ] }
                        setTitle={ setOrderTitle } setFilter={ setOrder }
                        currentTitle={ orderTitle } updatePage={ setCurrentPage } /> }
                    {/* instantiate a search bar on the model page which searches by name */ }
                    {<NewsSearchForm butSize="75" whichFilter="Search for News"
                        updatePage={ setCurrentPage } setTitle={ setSearch } /> }
                </Row>
            );
        }
        return ( <br></br> );
    }
    return (
        <>
            <div className="page">
                <Row className="justify-content-between modelHeader">
                    <h2>
                        <span className="modelPage">News</span>
                        <span className="divider">&ensp;|&ensp;</span>
                        <span className="pageDeclaration">Page { currentPage }</span>
                    </h2>
                    {/* Displays the filters and search bar for the model page, depending on the searchPage boolean */ }
                    { filterSearch( searchPage ) }
                </Row>
                <br></br><br></br>
                <Row className="no-gutter">
                    {/* Displays a row of the cardsRow list, which has up to 10 NewsCards */ }
                    { cardsRow }
                </Row>
                {/* Set up pagination at bottom of page */}
                {<Pagination currentWindow={currentWindow} numPages={numPages} currentPage={currentPage}
                updateWindow={setCurrentWindow} updatePage={setCurrentPage} />}
            </div>
        </>
    )
}

export default News;
