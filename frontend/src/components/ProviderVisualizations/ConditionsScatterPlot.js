import React from 'react';
import { ScatterChart, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip,
            Cell } from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
import useAxios from 'axios-hooks'
import '../../assets/styles/Visualizations.css';

const url = "https://healthygroceries.me/api/conditions";

export default function ConditionsScatterPlot() {
    const [ { data, loading } ] = useAxios( url );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }

    const parsedData = parseConditions(data);
    let graphData = [];
    for (let condition in parsedData) {
        graphData.push({name: parsedData[condition][0],
                        level: parsedData[condition][1],
                        numCases: parsedData[condition][2]});
    }

    const colors = scaleOrdinal(schemeCategory10).range();

    return (
        <div className="page">
            <ScatterChart
            width={500}
            height={500}
            >
                <CartesianGrid />
                <ZAxis type="category" dataKey="name" name="Conditions" />
                <XAxis type="number" ticks={[1, 2, 3, 4, 5]} domain={[0, 6]}
                        dataKey="level" name="Influence Level" />
                <YAxis type="number" dataKey="numCases" name="Number of Cases"
                        scale="log" domain={[2, 70000000]} allowDataOverflow />
                <Tooltip cursor={{ strokeDasharray: '3 3' }}
                      formatter={(value, name, props) => FormatValue(value) } />
                <Scatter name="A school" data={graphData} fill="#8884d8">
                    {graphData.map((entry, index) =>
                               <Cell key={"provScatterKey" + index}
                               fill={colors[entry["level"] % colors.length]}/>)}
                </Scatter>
            </ScatterChart>
        </div>
    );
}

// Parses the time of every article
function parseConditions(data) {
    let conditions = {};

    for (const index in data) {
        const condition = data[index];
        const levelAndCases = condition["influence"] + "-" + condition["cases"];
        if(!(levelAndCases in conditions)){
            conditions[levelAndCases] = [ condition["name"],
                                          condition["influence"],
                                          condition["cases"]     ];
        }
        else {
            conditions[levelAndCases][0] += ", " + condition["name"];
        }
    }
    return conditions;
}

function FormatValue(value){
	return (
  	<span>
    {typeof value == "string" ? value.split(',')[0] : value}
    {
    	typeof value == "string" && value.includes(',') ?
    	value.split(',').slice(1).map(element =>
    	    <span>,<br/>&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;{element}</span>)
    	:
      null
    }
  	</span>
  );
}
