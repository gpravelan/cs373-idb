import React from "react";
import Highlighter from "react-highlight-words";
import "../../assets/styles/Highlight.css";

const Highlight = (props) =>  {
    return <Highlighter
            highlightClassName="highlight-query"
            searchWords={props.searchQuery.trim().split(' ')}
            autoEscape={true}
            textToHighlight={props.word}
        />
};

export default Highlight;
