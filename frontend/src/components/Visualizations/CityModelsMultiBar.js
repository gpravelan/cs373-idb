import React from "react";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import {CityData} from "./CityData"

export default function CityBar () {

    // data preloaded for speed to avoid paginated API requests
    const unformattedData = CityData();

    let data = [];

    for (let city in unformattedData) {
        data.push({name: city, numVenues: unformattedData[city]})
    }

    return (
        <div className="scrollable">
            <BarChart width={5000} height={500} data={data}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis interval={0} height={125} angle={80} textAnchor="start"
                        dataKey="name" />
                <YAxis />
                <Tooltip formatter={(value, name, props) =>
                                    [value, "Number of venues"] } />
                <Bar dataKey="numVenues" fill="#c89664">
                    {data.map((entry, index) =>
                               <Cell key={"barKey" + index} fill={"#c89664"}/>)}
                </Bar>
            </BarChart>
        </div>
    );
}
