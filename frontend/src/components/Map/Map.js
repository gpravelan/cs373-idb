import React from 'react';

const google_api_key = "AIzaSyCKIzoBdK3VBHYVDH23oS1t_UBRWHdGSMw";
const google_api_url = "https://www.google.com/maps/embed/v1/place?key=" + google_api_key;

// Return a map to be displayed on a venue card or venue page
export default function DisplayMap (address, city) {
    const address_encoded = encodeURIComponent(address);
    const city_encoded = encodeURIComponent(city);
    return (<iframe
              title="map"
              className="card-img-top"
              src= {google_api_url + "&q=" + address_encoded + ", " + city_encoded}
              allowFullScreen>
            </iframe>);
}
