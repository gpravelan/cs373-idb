import React from 'react';
import { Link } from "react-router-dom";
import { Button, Container, Col, Row } from 'react-bootstrap';
import useAxios from "axios-hooks";
import ModelInformation, { ModelInformationListRow } from "../ModelInformation/ModelInformation";
import "../../assets/styles/ModelInstance.css"


//represents the complete instance of a news instance
//displays all available information rather than the key information displayed in NewsCards
//clicking the More Info button on a NewsCard redirects to this function
export default function NewsInstance ( { match } )
{
    //make a specific api call with the id of the news instance and store the result in data
    const [ { data, loading } ] = useAxios( `/api/only-one-singular-piece-of-news/${ match.params.id }` );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }

    let date_time = data[ "publication_date" ];

    if ( date_time )
    {
        let date_time_array = date_time.split( "-" );
        let calendar = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
        let month = calendar[ parseInt( date_time_array[ 1 ] ) - 1 ];
        let day = date_time_array[ 2 ].split( " " )[ 0 ];
        let year = date_time_array[ 0 ];
        let time = date_time_array[ 2 ].split( " " )[ 1 ].split( ":" );
        let marker = "a";
        let hour = parseInt( time[ 0 ] )
        if ( hour > 12 )
        {
            hour -= 12;
            marker = "p"
        } else if ( hour === 0 )
        {
            hour = 12;
        }
        let minute = time[ 1 ]
        date_time = hour + ":" + minute + marker + " " + month + " " + day + ", " + year;
    }

    let media = data[ "media" ];
    if ( !media )
    {
        //if the NewsInstance does not have media, then sets media to a noImage picture to make this
        //more clear to the user
        media = require( "../../assets/images/noImageAvailable.jpg" );
    }

    return (
        <>
            <div className="page">
                {/* Each NewsInstance will access and output the title, source, link, summary,
                    image, publication date, relevant venue, and related event links of the news*/}
                <Container fluid>
                    <Row>
                        <Col className="modelName">
                            {/* Display news title */ }
                            <div className="header">{ data[ "title" ] }</div>
                            <hr></hr>
                            <Button className="backButton">
                                <Link className="backText" to={ `/news/` } >Back to News</Link>
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="modelOverview">
                            {/* Display news media */ }
                            <img alt="" id="model-img" src={ media } />
                            {/* Display news source */ }
                            { ModelInformation( "Publication", data[ "source" ] ) }
                            {/* Display news publication date */ }
                            { ModelInformation( "Time", date_time ) }
                            {/* Display news link */ }
                            { ModelInformation( "Link", <a href={ data[ "link" ] }>View this article ></a> ) }
                            {/* Display news venue */ }
                            { ModelInformation( "Event Venue",
                                <Link to={ `/venues/${ data[ "venue" ][ "id" ] }` }>{ data[ "venue" ][ "name" ] } ></Link> ) }
                            {/* Display news summary */ }
                            { ModelInformation( "Summary", data[ "summary" ] ) }
                        </Col>
                        <Col className="mt-2">
                            {/* Display news's 15-most related events */ }
                            { ModelInformationListRow( "Relevant Events", data[ "relevant_events" ], "events" ) }

                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}
