import React from 'react';
import { Col, Row, Tab, Tabs} from 'react-bootstrap';
import ArticlePublishingTimePieChart from "./ArticlePublishingTimePieChart";
import CityModelsMultiBar from "./CityModelsMultiBar";
import EventsPerMonthLine from "./EventsPerMonthLine";
import ConditionsScatterPlot from "../ProviderVisualizations/ConditionsScatterPlot";
import FoodsBar from "../ProviderVisualizations/FoodsBar";
import StoreChainPie from "../ProviderVisualizations/StoreChainPie";
import "../../assets/styles/Visualizations.css"

const lineTitle = "Number of Events Scheduled in Each Month"
const pieTitle = "Time of Day Articles Are Published"
const barTitle = "Number of Venues in Major US Cities"
const pPieTitle = "Store Chain of Grocers"
const pBarTitle = "Number of Foods in Each Category"
const pScatterTitle = "Cases Per Condition, Grouped by Level of Dietary Influence"



export const Visualizations = ( props ) =>
{
    return (
        <div className="page">
            <h2 className="vis_page_name">Visualizations</h2>
            <br></br>
            <Tabs className="justify-content-around" defaultActiveKey="findeventsnearme">
                <Tab eventKey="findeventsnearme" title="Find Events Near Me">
                    <Row className="mt-5">
                        <Col>
                            <h3 className="vis_page_d3_title">{lineTitle}</h3>
                            { EventsPerMonthLine() }
                        </Col>
                        <Col>
                            <h3 className="vis_page_d3_title">{pieTitle}</h3>
                            { ArticlePublishingTimePieChart() }
                        </Col>
                    </Row>
                    <Row>
                        <h3 className="vis_page_d3_title mb-5">{barTitle}</h3>
                        { CityModelsMultiBar() }
                    </Row>
                </Tab>
                <Tab eventKey="healthygroceries" title="Healthy Groceries">
                    <Row className="mt-5">
                        <Col>
                            <h3 className="vis_page_d3_title">{pScatterTitle}</h3>
                            { ConditionsScatterPlot() }
                        </Col>
                        <Col>
                            <h3 className="vis_page_d3_title">{pPieTitle}</h3>
                            { StoreChainPie() }
                        </Col>
                    </Row>
                    <Row>
                        <h3 className="vis_page_d3_title mb-5">{pBarTitle}</h3>
                        { FoodsBar() }
                    </Row>
                </Tab>
            </Tabs>

        </div>
    )
}

export default Visualizations;
