import React from 'react';
import { Link } from "react-router-dom";
import { Button, Card, Row } from 'react-bootstrap/'
import Highlight from "../Highlight/Highlight";
import "../../assets/styles/Card.css"

//EventCard takes in data collected from one event object after an API call and displays
//a styled card with important event information highlighted on it, including images
export function EventCard ( { search, id, name, date_time, media, promoter } )
{
    if ( date_time )
    {
        let date_time_array = date_time.split( "-" );
        let calendar = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
        let month = calendar[ parseInt( date_time_array[ 1 ] ) - 1 ];
        let day = date_time_array[ 2 ].split( " " )[ 0 ];
        let year = date_time_array[ 0 ];
        let time = date_time_array[ 2 ].split( " " )[ 1 ].split( ":" );
        let marker = "a";
        let hour = parseInt( time[ 0 ] )
        if ( hour > 12 )
        {
            hour -= 12;
            marker = "p"
        } else if ( hour === 0 )
        {
            hour = 12;
        }
        let minute = time[ 1 ]
        date_time = hour + ":" + minute + marker + " " + month + " " + day + ", " + year;
    }
    promoter = promoter.toLowerCase();
    //highlights any model names with the search query passed in, if applicable
    let eventName = <Highlight searchQuery = {search} word = {name} />;
    return (
        <>
            <div>
            {/* Sets up the dimensions and styling of the card */}
            <Card className="align-items-center">
                <Card.Img variant="top" src= {media} />
                <Card.Body>
                    <div className="date">{ date_time }</div>
                    <Card.Title>{eventName}</Card.Title>
                    {/* outputs the args passed in with relevant identifiers */}
                        {/* <h6>Promoter: <small>{ promoter }</small></h6> */}
                    <div className="promoter">{ promoter }</div>

                        {/* Provides a More Info button which directs to an EventsInstance which has all information */ }

                </Card.Body>
                <Row className="justify-content-center">
                    <Button disabled id={ 'button' + id } >
                        <Link to={ `/events/${ id }` } className="moreInfo">More Info</Link>
                    </Button>
                </Row>
            </Card>

            </div>
        </>
    )
}

export default EventCard;
