import math
from db_handler import retrieve_page_and_count,\
                        retrieve_instance,\
                        retrieve_related_models,\
                        PAGE_SIZE


def retrieve_page_db(table_name, query_params):

    # Get data
    all_rows = retrieve_page_and_count(table_name, query_params)

    # Initialize pages and dictionary in case of no results
    total_pages = 0
    dict_list = []

    if len(all_rows):
        # Calculate total number of pages for frontend
        total_entries = all_rows[0][len(all_rows[0]) - 1]
        total_pages = math.ceil(total_entries / PAGE_SIZE)

        # Populate data dictionary with attributes received from DB
        if table_name == "venues":
            for row in all_rows:
                dict_list.append(create_venue_dict(row))
        elif table_name == "events":
            for row in all_rows:
                dict_list.append(create_event_dict(row))
        elif table_name == "articles":
            for row in all_rows:
                dict_list.append(create_news_dict(row))
        else:
            raise Exception("Unexpected error occurred: invalid table_name '{}'"
                            .format(table_name))

    return {'total_pages': total_pages, 'data': dict_list}


def retrieve_specific_db(table_name, row_id):

    # Get data for instance if any
    rows = retrieve_instance(table_name, row_id)

    if len(rows):
        row = rows[0]

        if table_name == "venues":
            formatted_row = create_venue_dict(row)
            # Add associated events to JSON model
            related_events = retrieve_related_models("events", row[7])
            formatted_related_events = []
            for event in related_events:
                formatted_related_events.append(create_event_dict(event))
            formatted_row["relevant_events"] = formatted_related_events
            # Add associated news to JSON model
            related_news = retrieve_related_models("articles", row[7])
            formatted_related_news = []
            for news in related_news:
                formatted_related_news.append(create_news_dict(news))
            formatted_row["relevant_news"] = formatted_related_news
        elif table_name == "events":
            formatted_row = create_event_dict(row)

            # Add associated venue to JSON model
            related_venue = retrieve_related_models("venues", row[7])[0]
            formatted_row["venue"] = create_venue_dict(related_venue)

            # Add associated news to JSON model
            related_news = retrieve_related_models("articles", row[7])
            formatted_related_news = []
            for news in related_news:
                formatted_related_news.append(create_news_dict(news))
            formatted_row["relevant_news"] = formatted_related_news
        elif table_name == "articles":
            formatted_row = create_news_dict(row)

            # Add associated venue to JSON model
            related_venue = retrieve_related_models("venues", row[7])[0]
            formatted_row["venue"] = create_venue_dict(related_venue)

            # Add associated events to JSON model
            related_events = retrieve_related_models("events", row[7])
            formatted_related_events = []
            for event in related_events:
                formatted_related_events.append(create_event_dict(event))
            formatted_row["relevant_events"] = formatted_related_events
        else:
            raise Exception("Unexpected error occurred: invalid table_name '{}'"
                            .format(table_name))

        return formatted_row
    raise Exception("Instance '{}' does not exist for table '{}'"
                    .format(id, table_name))


def create_venue_dict(row):
    venue_dict = {
        "id": row[0],
        "name": row[1],
        "address": row[2],
        "city": row[3],
        "zip_code": row[4],
        "parking_info": row[5],
        "accessible_seating_info": row[6]
    }
    return venue_dict


def create_event_dict(row):
    event_dict = {
        "id": row[0],
        "name": row[1],
        "link": row[2],
        "date_time": row[3],
        "media": row[4],
        "promoter": row[5],
        "special_info": row[6]
    }
    return event_dict


def create_news_dict(row):
    news_dict = {
        "id": row[0],
        "title": row[1],
        "source": row[2],
        "link": row[3],
        "media": row[4],
        "summary": row[5],
        "publication_date": row[6]
    }
    return news_dict

