from flask import Flask, request, render_template, Response
from controller import retrieve_page_db, retrieve_specific_db
from flask_cors import CORS
import json

static = "../frontend/build/static"
template = "../frontend/build/"

app = Flask(__name__, static_folder=static, template_folder=template)
CORS(app)
page_size = 10


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    return render_template("index.html")


@app.route('/api/')
def hello_world () :
    return "YAY IT WORKS!"


@app.route('/api/venues')
def venue_list():
    # TODO: make first element in return body number of pages.
    try:
        requested_venues = retrieve_page_db("venues", request.args)
        resp_body = json.dumps(requested_venues)
        mime = "application/json"
        return app.response_class(response=resp_body, mimetype=mime, status=200)
    except Exception as e:
        print(e)
        resp_body = "An internal error occured."
        mime = "text/html"
        return app.response_class(response=resp_body, mimetype=mime, status=500)


@app.route('/api/venue/<int:venue_id>')
def specific_venue(venue_id):
    try:
        resp_body = json.dumps(retrieve_specific_db("venues", venue_id))
        mime = "application/json"
        return app.response_class(response=resp_body, mimetype=mime, status=200)
    except Exception as e:
        print(e)
        resp_body = "An internal error occured."
        mime = "text/html"
        return app.response_class(response=resp_body, mimetype=mime, status=500)


@app.route('/api/events')
def event_list():
    try:
        requested_venues = retrieve_page_db("events", request.args)

        resp_body = json.dumps(requested_venues)
        mime = "application/json"
        return app.response_class(response=resp_body, mimetype=mime, status=200)
    except Exception as e:
        print(e)
        resp_body = "An internal error occured."
        mime = "text/html"
        return app.response_class(response=resp_body, mimetype=mime, status=500)


@app.route('/api/event/<int:event_id>')
def specific_event(event_id):
    try:
        resp_body = json.dumps(retrieve_specific_db("events", event_id))
        mime = "application/json"
        return app.response_class(response=resp_body, mimetype=mime, status=200)
    except Exception as e:
        print(e)
        resp_body = "An internal error occured."
        mime = "text/html"
        return app.response_class(response=resp_body, mimetype=mime, status=500)


@app.route('/api/news')
def news_list():
    try:
        requested_venues = retrieve_page_db("articles", request.args)

        resp_body = json.dumps(requested_venues)
        mime = "application/json"
        return app.response_class(response=resp_body, mimetype=mime, status=200)
    except Exception as e:
        print(e)
        resp_body = "An internal error occured."
        mime = "text/html"
        return app.response_class(response=resp_body, mimetype=mime, status=500)


@app.route('/api/only-one-singular-piece-of-news/<int:news_id>')
def specific_news(news_id):
    try:
        resp_body = json.dumps(retrieve_specific_db("articles", news_id))
        mime = "application/json"
        return app.response_class(response=resp_body, mimetype=mime, status=200)
    except Exception as e:
        print(e)
        resp_body = "An internal error occured."
        mime = "text/html"
        return app.response_class(response=resp_body, mimetype=mime, status=500)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, threaded=True, debug=True)
