import React from 'react';
import {PieChart, Cell, Legend, Pie, Tooltip} from "recharts";
import useAxios from 'axios-hooks'
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';

const url = "https://healthygroceries.me/api/stores";

export default function ProviderGrocersPie() {
    const [ { data, loading } ] = useAxios( url );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }

    const colors = scaleOrdinal(schemeCategory10).range();

    const parsedData = parseGrocers(data);
    let graphData = [];
    for (let grocer in parsedData) {
        graphData.push({name: grocer, numStores: parsedData[grocer]})
    }
    return (
        <div className="page">
            <PieChart width={500} height={500}>
                <Pie dataKey="numStores" data={graphData} cx={250} cy={150}
                        outerRadius={150} fill="#c89664" >
                    {graphData.map((entry, index) =>
                                        <Cell key={"provPieKey" + index}
                                        fill={colors[index % colors.length]}/>)}
                </Pie>
                <Tooltip />
                <Legend iconSize={50}/>
            </PieChart>
        </div>
    )
}

// Parses the time of every article
function parseGrocers(data) {
    let grocers = {};

    for (const index in data) {
        const grocer = data[index];
        let grocerName = grocer["chain"];

        if(!(grocerName in grocers)) {
            grocers[grocerName] = 0;
        }

        ++grocers[grocerName];
    }

    return grocers;
}
