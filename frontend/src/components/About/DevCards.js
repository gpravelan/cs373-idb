import React from 'react';
import { Row, Card } from 'react-bootstrap/';


export default function DevCardRow(developers){
    return (
        <Row className="justify-content-around">
              {developers.map(listitem => (
                  DevCard(listitem[0],
                          listitem[1],
                          listitem[2],
                          listitem[3],
                          listitem[4],
                          listitem[5],
                          listitem[6])
              ))}
        </Row>
    );
}

function DevCard(name, photo, bio, authorName, username, tests, linkedIn){
    return (
        <Card className="developer">
            <Card.Img className="developer-image" src={photo}/>
            <Card.Body>
                <Card.Title className="about-subheader">{name}</Card.Title>
                <p className="about-subheader-accent">Fullstack</p>
                <p className="about-text-accent">{bio}</p>
                <a href={linkedIn}>LinkedIn</a>
                <hr></hr>
                <p className="about-subheader-accent">
                    <span className="commitStat" id={ authorName }>
                        0
                    </span> Commits</p>
                <p className="about-subheader-accent">
                    <span className="issueStat" id={ username }>
                    0
                    </span> Issues</p>
                <p className="about-subheader-accent">{tests} Tests</p>
            </Card.Body>
        </Card>
        );
}
