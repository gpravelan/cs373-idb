import React from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid,
            Tooltip } from 'recharts';
import useAxios from 'axios-hooks'
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';


const colors = scaleOrdinal(schemeCategory10).range();
const url = "https://healthygroceries.me/api/foods";

export default function FoodsBar() {
    const [ { data, loading } ] = useAxios( url );
    if ( loading )
    {
        return (
            <>
                <div className="load">
                    <hr /><hr /><hr /><hr />
                </div>
            </>
        )
    }

    const parsedData = parseCategories(data);
    let graphData = [];
    for (let category in parsedData) {
        graphData.push({name: category, numFoods: parsedData[category]})
    }
    return (
        <div className="scrollable">
            <BarChart width={2000} height={500} data={graphData}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Bar dataKey="numFoods" fill="#8884d8" >
                    {graphData.map((entry, index) =>
                                    <Cell key={"provBarKey" + index}
                                    fill={colors[1]}/>)}
                </Bar>
            </BarChart>
        </div>
    )
}

// Parses the time of every article
function parseCategories(data) {
    let categories = {};

    for (const index in data) {
        const food = data[index];
        let category = food["food group"];

        if(!(category in categories)) {
            categories[category] = 0;
        }

        ++categories[category];
    }

    return categories;
}
