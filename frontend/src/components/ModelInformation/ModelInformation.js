import React from 'react';
import {Link} from "react-router-dom";

// Return a row to be displayed on an individual model's instance page
export default function ModelInformation (title, content) {
    return (
        <div className="info-row">
            <div className="info-title">{title}</div>
            <div className="info-body">{content}</div>
        </div>
    )
}

// Return a row listing related elements on an individual model's instance page
export function ModelInformationListRow (title, list, route) {
    return (
        <div className="info-row">
            <div className="info-title">{ title }</div>
            <div className="info-body related-list">
                <ul>
                    {
                        /* loops through list and outputs links for each element  */
                        list.map(link =>
                            <li key={link.id}>
                                <Link to={`/${route}/${link.id}`}>{route === "news" ? link.title : link.name} </Link>
                            </li>
                        )
                    }
                </ul>
            </div>
        </div>
    )
}
