import React from 'react';
import { Dropdown } from 'react-bootstrap/'
import NewsSearchForm from './NewsSearchForm.js'

//NewsDropDown represents a single dropdown menu and has the dropdown's selectable values
//each NewsDropDown has an optional textbox placeholder, a name of the dropdown, a color, and filters list
//setTitle and setFilter are callable state functions which will re render main page when changed
export function NewsDropDown ( { a, name, color, filters, setTitle, setFilter, currentTitle, updatePage } )
{

    function update ( updatedTitle )
    {
        if ( updatedTitle === "all" )
        {
            //user selected the all option, so reset the appropriate filter to default name
            updatedTitle = "none";
        }
        if ( name === 'source' || name === 'venue' || name === 'publication date' )
        {
            //user selected source or venue or publication filter, updates API value and dropdown title
            setTitle( updatedTitle === "none" ? name : updatedTitle );
            setFilter( updatedTitle );
        } else if ( name === 'media' )
        {
            //user selected media filter, updates media API value and media dropdown title
            let s = "none";
            if ( updatedTitle === "has media" )
            {
                s = 'true';
            } else if ( updatedTitle === "no media" )
            {
                s = 'false';
            }
            setTitle( updatedTitle === "none" ? name : updatedTitle );
            setFilter( s );
        } else if ( name === 'sort by' )
        {
            //user selected sort filter, updates sort API value and sort dropdown title
            setTitle( updatedTitle === "none" ? name : 'sorting by ' + updatedTitle )
            setFilter( updatedTitle.replace( / /g, "_" ) );
        } else if ( name === 'related events' )
        {
            //user selected number of related events, updated related events API value and dropdown
            let s = "none";
            if ( updatedTitle === "less than 5 related events" )
            {
                s = "=<5"
            } else if ( updatedTitle === "more than 5 related events" )
            {
                s = "=>5"
            } else if ( updatedTitle === "equal to 5 related events" )
            {
                s = "==5"
            }
            setTitle( updatedTitle === "none" ? name : updatedTitle );
            setFilter( s );
        }
        //reset the current page to 1 for api call since user selected a filter
        updatePage( 1 );
    }
    //declare a list of dropDown options for this drop down
    let dropDowns = [];
    for ( let i = 0; i < filters.length; i++ )
    {
        if ( i === 0 )
        {
            //the first option for all dropdowns is "all" which effectively resets that filter and search query
            dropDowns.push( <Dropdown.Item key={ filters[ i ] } onClick={ () =>
            { update( "all" ) } }>{ filters[ i ] }</Dropdown.Item> );
        } else
        {
            //remaining filters, which are passed in through filters, are looped through and added as dropdown items to menu
            dropDowns.push( <Dropdown.Item key={ filters[ i ] } onClick={ () =>
            { update( filters[ i ] ) } }>{ filters[ i ] }</Dropdown.Item> );
        }
    }
    //returns a toggleable drop down menu button with filters, updated menu name, and optional search
    return (
        <Dropdown>
            <Dropdown.Toggle id="dropdown-basic">{ currentTitle }</Dropdown.Toggle>
            <Dropdown.Menu>
                {/* in the dropdown menu, pass in the list of drop downs we made after looping through filters */ }
                { dropDowns }
                {/* depending on what was passed in as a, appends the corrrect search form to menu if applicable */ }
                { a === 'Search for a Source' ? ( <NewsSearchForm butSize="40"
                    updatePage={ updatePage } setTitle={ setTitle }
                    setFilter={ setFilter } whichFilter={ a } /> ) : void ( 0 ) }
                { a === 'Search for a Venue' ? ( <NewsSearchForm butSize="40"
                    updatePage={ updatePage } setTitle={ setTitle }
                    setFilter={ setFilter } whichFilter={ a } /> ) : void ( 0 ) }
                { a === 'YYYY-MM-DD' ? ( <NewsSearchForm butSize="40"
                    updatePage={ updatePage } setTitle={ setTitle }
                    setFilter={ setFilter } whichFilter={ a } /> ) : void ( 0 ) }
            </Dropdown.Menu>
        </Dropdown>
    )
}

export default NewsDropDown;
