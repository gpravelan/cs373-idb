import React from 'react';
import { Dropdown } from 'react-bootstrap/'
import VenueSearchForm from './VenueSearchForm.js'

//VenueDropDown represents a single dropdown menu and has the dropdown's selectable values
//each VenueDropDown has an optional textbox placeholder, a name of the dropdown, a color, and filters list
//setTitle and setFilter are callable state functions which will re render main page when changed
export function VenueDropDown ( { a, name, color, filters, setTitle, setFilter, currentTitle, updatePage } )
{

    function update ( updatedTitle )
    {
        if ( updatedTitle === "all" )
        {
            //user selected the all option, so reset the appropriate filter to default name
            updatedTitle = "none";
        }
        if ( name === 'city' || name === 'zip code' )
        {
            //user selected city filter, updates city API value and city dropdown title OR
            //user selected zip code filter, updates zip code API value and zip code dropdown title
            setTitle( updatedTitle === "none" ? name : updatedTitle );
            setFilter( updatedTitle );
        } else if ( name === 'parking info' )
        {
            //user selected parking filter, updates parking API value and parking dropdown title
            let s = "none";
            if ( updatedTitle === "has parking info" )
            {
                s = 'true';
            } else if ( updatedTitle === "no parking info" )
            {
                s = 'false';
            }
            setTitle( updatedTitle === "none" ? name : updatedTitle );
            setFilter( s );
        } else if ( name === 'seating info' )
        {
            //user selected seating filter, updates seating API value and seating dropdown title
            let s = "none";
            if ( updatedTitle === "has seating info" )
            {
                s = 'true';
            } else if ( updatedTitle === "no seating info" )
            {
                s = 'false';
            }
            setTitle( updatedTitle === "none" ? name : updatedTitle )
            setFilter( s );
        } else if ( name === 'sort by' )
        {
            //user selected sort filter, updates sort API value and sort dropdown title
            setTitle( updatedTitle === "none" ? name : 'sorting by ' + updatedTitle )
            setFilter( updatedTitle.replace( / /g, "_" ) );
        }
        //reset the current page to 1 for api call since user selected a filter
        updatePage( 1 );
    }
    //declare a list of dropDown options for this drop down
    let dropDowns = []
    for ( let i = 0; i < filters.length; i++ )
    {
        if ( i === 0 )
        {
            //the first option for all dropdowns is "all" which effectively resets that filter and search query
            dropDowns.push( <Dropdown.Item key={ filters[ i ] } onClick={ () =>
            { update( "all" ) } }>{ filters[ i ] }</Dropdown.Item> );
        } else
        {
            //remaining filters, which are passed in through filters, are looped through and added as dropdown items to menu
            dropDowns.push( <Dropdown.Item key={ filters[ i ] } onClick={ () =>
            { update( filters[ i ] ) } }>{ filters[ i ] }</Dropdown.Item> );
        }
    }

    //returns a toggleable drop down menu button with filters, updated menu name, and optional search
    return (
        <Dropdown>
            <Dropdown.Toggle id="dropdown-basic">{ currentTitle }</Dropdown.Toggle>
            <Dropdown.Menu>
                {/* in the dropdown menu, pass in the list of drop downs we made after looping through filters */ }
                { dropDowns }
                {/* depending on what was passed in as a, appends the corrrect search form to menu if applicable */ }
                { a === 'Search for a City' ? ( <VenueSearchForm butSize="40"
                    updatePage={ updatePage } setTitle={ setTitle }
                    setFilter={ setFilter } whichFilter={ a } /> ) : void ( 0 ) }
                { a === 'Search for a Zip Code' ? ( <VenueSearchForm butSize="40"
                    updatePage={ updatePage } setTitle={ setTitle }
                    setFilter={ setFilter } whichFilter={ a } /> ) : void ( 0 ) }
            </Dropdown.Menu>
        </Dropdown>
    )
}

export default VenueDropDown;
